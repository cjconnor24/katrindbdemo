
package net.katrinhartmann.servlet;

import net.katrinhartmann.dbdemo.db.H2Person;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;


public class Runner {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(Runner.class);

    private static final int PORT = 9000;

    private final H2Person h2Person;

    public Runner() {
        h2Person = new H2Person();
    }

    public void start() throws Exception {
        Server server = new Server(PORT);

        ServletContextHandler handler = new ServletContextHandler(server, "/", ServletContextHandler.SESSIONS);
        handler.setContextPath("/");
        handler.setInitParameter("org.eclipse.jetty.servlet.Default." + "resourceBase", "src/main/resources/webapp");

        handler.addServlet(new ServletHolder(new PersonServlet(h2Person)), "/index.html");
        handler.addServlet(new ServletHolder(new PersonServlet(h2Person)), "/add"); // we post to here

        // Added this servlet for returning all persons
        handler.addServlet(new ServletHolder(new PersonsServlet(h2Person)), "/view");

        DefaultServlet ds = new DefaultServlet();
        handler.addServlet(new ServletHolder(ds), "/");

        server.start();
        LOG.info("Server started, will run until terminated");
        server.join();
    }

    public static void main(String[] args) {
        try {
            LOG.info("starting dbdemo");
            Locale.setDefault(Locale.UK);
            new Runner().start();
        } catch (Exception e) {
            LOG.error("Unexpected error running dbdemo: " + e.getMessage());
        }
    }
}
