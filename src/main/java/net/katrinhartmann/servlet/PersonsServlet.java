
package net.katrinhartmann.servlet;

import lombok.Data;
import net.katrinhartmann.dbdemo.db.H2Person;
import net.katrinhartmann.dbdemo.model.Person;
import net.katrinhartmann.util.mustache.MustacheRender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet for view all persons in the DB
 */
public class PersonsServlet extends HttpServlet{


    private final H2Person h2Person;
    private final MustacheRender mustache;

    /**
     * Initiate instance of H2Person
     * @param h2Person
     */
    public PersonsServlet(H2Person h2Person) {
        mustache = new MustacheRender();
        this.h2Person = h2Person;
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Retrieve the people from the database
        List<Person> persons = h2Person.findPersons();

        // Pass persons to the mustache template
        String html = mustache.render("display.mustache", persons);

        // Set the content type and status code and render/write the response
        response.setContentType("text/html");
        response.setStatus(200);
        response.getOutputStream().write(html.getBytes(Charset.forName("utf-8")));
    }

}

