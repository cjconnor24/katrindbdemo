# Mustache Rendering Issue
Hi Katrin, as you saw in the labs, it doesn't look like I can get mustache to render the details that I'm trying to pass through.

Below is a quick run down of what I did.

## Added new `loadPersons` method to `H2Person.java` class
```java
    public List<Person> loadPersons(){
    
        // SELECT ALL ROWS IN PERSON TABLE AND MAP TO PERSON OBJECT
        final String LIST_PERSONS = "SELECT * FROM person";
        List<Person> results = new ArrayList<>();

        try (PreparedStatement ps = connection.prepareStatement(LIST_PERSONS)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                results.add(new Person(rs.getString(1), rs.getString(2), rs.getString(3)));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return results;
    }
```

## Added new `PersonsServlet.java`
All this does is retrieve from the message above and try to render to the `display.mustache` mustache template
```java
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Retrieve the people from the database
        List<Person> persons = h2Person.findPersons();

        // Pass persons to the mustache template
        String html = mustache.render("display.mustache", persons);

        // Set the content type and status code and render/write the response
        response.setContentType("text/html");
        response.setStatus(200);
        response.getOutputStream().write(html.getBytes(Charset.forName("utf-8")));
    }
```

## Added `display.mustache` template
All this does is try to render the results in an unordered list
```html
    <!--SHOULD LOOP THROUGH ALL PERSONS IN PERSONS LIST AND OUTPUT - BUT DOESNT -->
    <ul>
        {{#persons}}
        <li>{{first}} {{last}} <a href="mailto:{{email}}" target="_blank"> Email {{first}} {{last}}</a></li>
        {{/persons}}
    </ul>
```

## Added the  servlet to the handler in `Runner,java` class
```java
        handler.addServlet(new ServletHolder(new PersonsServlet(h2Person)), "/view");
```

# The Issue
Unfortunately, all I get in the source is a `<ul>` tag with nothing inside. Tried a few things but can't seem to get it to render the results.

Having debugged, I am getting back the results from the database - it does show when I inspect in IntelliJ.

----------------

# Simple Database Demo

This is a simple example to show how to set up a database using Java Servlets

The code is licensed under the GPL Version 3.

## Requirements

To run it you need [Java 8][2], [Maven][3] and (optionally) [Intellij][4] for which there is a project set up.

# Using Git
    
The repository is stored in Git. We use branches to set up the various stages of development.
This initial version is on the `initial` branch.  You can see further branches as we go.
To get started, run the command

    git checkout initial

To get the initial state of the repository.

# Get started

You must have Java and Maven installed and can run Maven with the <code>mvn</code> command at a prompt.
To check this get up a command prompt and type <code>mvn --version</code> (note two hyphens).  You should
see a version number and other stuff about the Java version. You should have at least version 7 of Java.  
If you use Java 8 that's OK, but note that the Maven POM asks for Java 7, and Java 8 or higher doesn't run
on AppEngine.

Once Maven is installed then simply type

    mvn clean package exec:java

The first time this may take a while as a lot of dependencies need to be installed.  Future runs will be quicker.

By default you are running from the address `http://localhost:9000` on the development server. All the paths
below are relative to this, which may differ for you if you change it.

Now open a web browser and go to: `http://localhost:9000/index.html`. You will see a the web page:
    
## Technology stack in the starter

The starter uses [Jetty][1] to run an embedded web server.

The main class is `Runner`. This starts an embedded Jetty Server. 
 
# Templates

Its handy to use templates when creating a website.  A template get instantiated by a set of variables
which are computed or pulled from a database.

We use [Mustache][7] as our templating mechanism.  Its simple, and has the advantage
of being usable on the client side as well, so if we move to a client-based system we won't have to
redo the templates.

The `PersonServlet` servlet runs a template, which displays a form and a count of `Person`s
As well as the mustache templating we've set up the default servlet to serve static content, so we can
load styles, scripts and static files.


----

With a Java servlet the standard way to implement functionality like this is with  

[1]: https://eclipse.org/jetty/
[2]: http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
[3]: http://maven.apache.org/download.cgi
[4]: http://www.jetbrains.com/idea/
